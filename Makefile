# Commands
MKDIR := mkdir -p

# General compiler options
CFLAGS := -Wall -Wextra -Wpedantic

# Directories
SRCDIR := src
INCDIR := $(SRCDIR)/inc
BUILDDIR := build
OUTDIR := out
TESTDIR := $(OUTDIR)/tests
CREATEDIRS := $(BUILDDIR) $(OUTDIR) $(TESTDIR)

# Inputs
OBJSRC := $(shell find $(SRCDIR)/obj -name *.c)
TESTSRC := $(shell find $(SRCDIR)/test -name *.c)

# Outputs
OBJ := $(patsubst $(SRCDIR)/obj/%.c,$(BUILDDIR)/%.o,$(OBJSRC))
TESTS := $(patsubst $(SRCDIR)/test/%.c,$(TESTDIR)/%,$(TESTSRC))

.PHONY: help sharedlib staticlib tests
# CLI targets
help:
	$(info TARGETS: help sharedlib staticlib tests clean)

sharedlib: $(OUTDIR)/libargmapper.so

# TODO: not implemented
staticlib: $(OUTDIR)/libargmapper.a

tests: CFLAGS += -g
tests: $(TESTS)

clean:
	$(RM) -r $(CREATEDIRS)

# Recipes
$(OUTDIR)/%.so: CFLAGS += -fpic
$(OUTDIR)/%.so: $(BUILDDIR)/%.o | $(CREATEDIRS)
	$(CC) -shared -o $@ $^
# TODO: static lib

$(TESTDIR)/%: $(SRCDIR)/test/%.c $(OBJ) | $(CREATEDIRS)
	$(CC) $(CFLAGS) -I$(INCDIR) -o $@ $^

$(BUILDDIR)/%.o: $(SRCDIR)/obj/%.c | $(CREATEDIRS)
	$(CC) -c $(CFLAGS) -I$(INCDIR) -o $@ $^

$(CREATEDIRS):
	$(MKDIR) $@
