#ifndef _LIBARGMAPPER_H
#define _LIBARGMAPPER_H
//Provides a program CLI specification, similar in some ways to
//argparse Python module.
//Usage: 
//  struct argmap_cli cli = default_argmap_cli();
//  ...

//Allows 0 arguments to a name
#define ARGMAPPER_FLAG_NO_ARGS      0x0
//Allows (and requires) a single argument to name
#define ARGMAPPER_FLAG_SINGLE_ARG   0x1
//Allows any number (N) of arguments to a particular name
#define ARGMAPPER_FLAG_NARGS        0x2

struct arg_pair {
  //Name of argument specifier, ie. -f or --foo or foo
  char *key;
  //Number of values to this key
  int count;
  //Argument value, ie. bar or bar.txt
  char **values;
};
struct arg_pair blank_arg_pair(void);
void destroy_arg_pair(struct arg_pair p);
//Set the key string to key
int arg_pair_set_key(struct arg_pair *p, const char *key);
//Add a value string (value) and increment count
int arg_pair_add_value(struct arg_pair *p, const char *value);

struct arg_map {
  //Number of argument pairs in struct
  int count;
  //{ key: value(s) } mappings, see above
  struct arg_pair *pairs;
};
struct arg_map blank_arg_map(void);
void destroy_arg_map(struct arg_map m);
//Add a pre-built struct arg_pair to the map
int arg_map_add_pair(struct arg_map *m, struct arg_pair p);

struct argmap_cli {
  //Number of argument specifiers (currently)
  int count;
  //Argument strings ie. -f, foo, --foo, --bar, all are valid and equal
  //- and -- before a specifier do not do anything special
  char **allowed_args;
  //If an argument is not specified, a string can be copied by default
  //as if it was specified
  char **default_args;
  //Provide different behavior for an argument specifier, such as required
  int *flags;
  //Ends special parsing of command line arguments, ie. -- in many programs
  char *terminator;
};
//Return a new, completely empty argmap_cli
struct argmap_cli blank_argmap_cli(void);
//Return a mostly empty argmap_cli with a few defaults, such as "--"
//terminator
struct argmap_cli default_argmap_cli(void);
//Properly cleanup (free) a structure however constructed.
void destroy_argmap_cli(struct argmap_cli cli);
//Add an argument to the CLI
int argmap_cli_add_arg(
    struct argmap_cli *cli, const char *name, const char *default_value,
    int flags);
//Get the index of an argument, or -1 if it does not exist in the CLI
int argmap_cli_index(struct argmap_cli cli, const char *name);
//Parse argc, argv as received from command line into a struct arg_map
//according to the specification in struct argmap_cli
//Non-destructive to argv
struct arg_map argmap_cli_parse_args(
    struct argmap_cli cli, int argc, const char *argv[]);



#endif
