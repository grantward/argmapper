#ifndef _ARGMAPPER_TESTKIT_H
#define _ARGMAPPER_TESTKIT_H

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define ASSERT(expr) do {\
  if (!(expr)) {\
    fprintf(stderr, "%s:%d: FAILED ASSERTION:\n", __FILE__, __LINE__);\
    fprintf(stderr, "%s\n", #expr);\
    exit(1);\
  }\
} while (0)

#define ASSERT_FALSE(expr) ASSERT(!(expr))
#define ASSERT_0(expr) ASSERT(0 == (expr))
#define ASSERT_NOT_0(expr) ASSERT(0 != (expr))
#define ASSERT_STR_EQ(str_a, str_b) ASSERT_0(strcmp(str_a, str_b))


#endif
