#include "libargmapper.h"
#include "testkit.h"

#include <stdio.h>

int main(void) {
  int ret = 0;
  //Test arg_map / arg_pair
  struct arg_map map = blank_arg_map();
  struct arg_pair p = blank_arg_pair();
  ASSERT_0(arg_pair_set_key(&p, "hello"));
  ASSERT_0(arg_pair_add_value(&p, "world"));
  ASSERT_STR_EQ(p.key, "hello");
  ASSERT_STR_EQ(p.values[0], "world");
  ASSERT_0(arg_map_add_pair(&map, p));
  destroy_arg_map(map);
  //Test argmap_cli
  struct argmap_cli cli = default_argmap_cli();
  ASSERT_STR_EQ(cli.terminator, "--");
  ASSERT_0(argmap_cli_add_arg(&cli, "-foo", "bar", 0));
  ASSERT_STR_EQ(cli.allowed_args[0], "-foo");
  ASSERT_STR_EQ(cli.default_args[0], "bar");
  ASSERT_0(cli.flags[0]);
  destroy_argmap_cli(cli);
  return ret;
}
