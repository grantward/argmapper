#include "libargmapper.h"

#include <stdlib.h>
#include <string.h>

//Internal functions
static char *_next_arg(
    struct argmap_cli cli, int start, int argc, const char *argv[]);


//struct arg_pair functions
struct arg_pair blank_arg_pair(void) {
  struct arg_pair ret = { 0 };
  return ret;
}

void destroy_arg_pair(struct arg_pair p) {
  free(p.key);
  for (int i=0; i<p.count; i++) {
    free(p.values[i]);
  }
  free(p.values);
}

int arg_pair_set_key(struct arg_pair *p, const char *key) {
  int ret = 0;
  if (p && key) {
    if (p->key) {
      free(p->key);
    }
    p->key = strdup(key);
  } else {
    //NULL pair/key, do not set
    ret = 1;
  }
  return ret;
}

int arg_pair_add_value(struct arg_pair *p, const char *value) {
  int ret = 0;
  if (p && value) {
    p->count++;
    char **temp = NULL;
    if (p->values) {
      temp = reallocarray(p->values, p->count, sizeof(char *));
    } else {
      temp = calloc(p->count, sizeof(char *));
    }
    if (temp) {
      //Alloc / realloc successful, set p.values with new array
      p->values = temp;
      p->values[p->count-1] = strdup(value);
    } else {
      //Memory failure
      p->count--;
      ret = -2;
    }
  } else {
    //NULL value, do not add
    ret = -1;
  }
  return ret;
}

//struct arg_map functions
struct arg_map blank_arg_map(void) {
  struct arg_map ret = { 0 };
  return ret;
}

void destroy_arg_map(struct arg_map m) {
  for (int i=0; i<m.count; i++) {
    destroy_arg_pair(m.pairs[i]);
  }
  free(m.pairs);
}

int arg_map_add_pair(struct arg_map *m, struct arg_pair p) {
  int ret = 0;
  if (m) {
    m->count++;
    struct arg_pair *temp = NULL;
    if (m->pairs) {
      temp = reallocarray(m->pairs, m->count, sizeof(*temp));
    } else {
      temp = calloc(m->count, sizeof(*temp));
    }
    if (temp) {
      m->pairs = temp;
      m->pairs[m->count-1] = p;
    } else {
      //Memory failure
      m->count--;
      ret = -2;
    }
  } else {
    //NULL map
    ret = -1;
  }
  return ret;
}

//struct argmap_cli functions
struct argmap_cli blank_argmap_cli(void) {
  struct argmap_cli ret = { 0 };
  return ret;
}

struct argmap_cli default_argmap_cli(void) {
  struct argmap_cli ret = blank_argmap_cli();
  ret.terminator = strdup("--");
  return ret;
}

void destroy_argmap_cli(struct argmap_cli cli) {
  for (int i=0; i<cli.count; i++) {
    free(cli.allowed_args[i]);
    free(cli.default_args[i]);
  }
  free(cli.allowed_args);
  free(cli.default_args);
  free(cli.flags);
  free(cli.terminator);
}

int argmap_cli_add_arg(
    struct argmap_cli *cli, const char *name, const char *default_value,
    int flags)
{
  int ret = 0;
  if (cli && name) {
    cli->count++;
    char **temp_args = NULL;
    char **temp_defaults = NULL;
    int *temp_flags = NULL;
    if (cli->count > 1) {
      //Array exists, resize it
      temp_args = reallocarray(cli->allowed_args, cli->count, sizeof(char *));
      temp_defaults = reallocarray(
          cli->default_args, cli->count, sizeof(char *));
      temp_flags = reallocarray(cli->flags, cli->count, sizeof(int *));
    } else {
      //Array does not exist, allocate it
      temp_args = calloc(cli->count, sizeof(char *));
      temp_defaults = calloc(cli->count, sizeof(char *));
      temp_flags = calloc(cli->count, sizeof(char *));
    }
    if (temp_args && temp_defaults && temp_flags) {
      //Successful allocation
      cli->allowed_args = temp_args;
      cli->default_args = temp_defaults;
      cli->flags = temp_flags;
      cli->allowed_args[cli->count-1] = strdup(name);
      if (default_value) {
        cli->default_args[cli->count-1] = strdup(default_value);
      }
      cli->flags[cli->count-1] = flags;
    } else {
      //Memory failure
      ret = -2;
      free(temp_args);
      free(temp_defaults);
      free(temp_flags);
    }
  } else {
    //NULL value where not allowed, invalid
    ret = -1;
  }
  return ret;
}

int argmap_cli_index(struct argmap_cli cli, const char *name) {
  //Default to not found or error (ie. NULL name)
  int ret = -1;
  if (name) {
    for (int i=0; i<cli.count; i++) {
      if (0 == strcmp(cli.allowed_args[i], name)) {
        ret = i;
        break;
      }
    }
  }
  return ret;
}

struct arg_map argmap_cli_parse_args(
    struct argmap_cli cli, int argc, const char *argv[])
{
  struct arg_map ret = blank_arg_map();
  int error = 0;
  const char *next = NULL;
  for (int i=1; i<argc; i++) {
    //Iterate through args ignoring argv[0] (usually program name)
    struct arg_pair pair = blank_arg_pair();
    int index = argmap_cli_index(cli, argv[i]);
    //TODO: strict mode will cause error
    //TODO: determine method to pass remaining args to caller
    if (0 <= index) {
      //argument name/flag is valid, begin processing
      //TODO: error checking
      error = arg_pair_set_key(&pair, argv[i]);
      if (cli.flags[i] & ARGMAPPER_FLAG_SINGLE_ARG) {
        //Allow (and require) a single argument
        if ((next=_next_arg(cli, i, argc, argv))) {
          error = arg_pair_add_value(&pair, next);
          i++;
        }
      } else if (cli.flags[i] & ARGMAPPER_FLAG_NARGS) {
        //Allow 0+ arg(s)
        for (next=_next_arg(cli, i, argc, argv);
             next;
             next=_next_arg(cli, ++i, argc, argv))
        {
          //Consume args until something from CLI appears or end
          error = arg_pair_add_value(&pair, next);
        }
      }
      //Add pair to map
      error = arg_map_add_pair(&ret, pair);
    }
  }
  return ret;
}

//Internal function to retrieve the next argument from argv not in cli
static char *_next_arg(
    struct argmap_cli cli, int start, int argc, const char *argv[])
{
  char *ret = NULL;
  int next = start+1;
  if (next < argc) {
    if (-1 == argmap_cli_index(cli, argv[next])) {
      //Not in cli, return it
      ret = (char *)(argv[next]);
    }
  }
  return ret;
}
